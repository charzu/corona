mkdir -p data/sdf
wget -qO- https://cactus.nci.nih.gov/download/nci/NCI-Open_2012-05-01.sdf.gz | \
    gunzip -c | csplit -s -f data/sdf/lig_ -b '%010d.sdf' - '/^\$\$\$\$/+1' '{*}'

mkdir -p data/moses
wget -qO- https://media.githubusercontent.com/media/molecularsets/moses/master/data/dataset_v1.csv | \
    sed '1d' | cut -f1 -d',' > data/moses/moses.smi