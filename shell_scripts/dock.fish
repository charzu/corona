mkdir -p data/results/

for i in data/pdbqt/*pdbqt
    set outdpf (echo $i | sed 's pdbqt configs ;s pdbqt dpf ')
    set lignum (echo $i | cut -f3 -d'/' | cut -f1 -d'.')

    # prepare configs for autodock
    ~/bin/pythonsh ~/opt/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24/prepare_dpf42.py \
        -l $i \
        -r data/receptor/6lu7_rec_autopsf.pdbqt \
        -o $outdpf \
        -i data/configs/screen.dpf \
        -p ga_num_evals=100000 \
        -p ga_pop_size=150 \
        -p ga_num_generations=27000

    # correct paths
    sed -i "s 6lu7_rec_autopsf data/receptor/maps/6lu7_rec_autopsf ;\
            s $lignum data/pdbqt/$lignum " $outdpf

    # dock
    sem -j -1 timeout 60s autodock4 -p $outdpf -l data/results/$lignum\.dlg
end
