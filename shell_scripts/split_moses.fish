mkdir data/split_moses
cd data/split_moses
split -l 10000 -a 1 -d --additional-suffix=.smi data/moses/moses.smi

for i in (seq 0 193)
                             mkdir -p $i
                             set num (printf "%03d" $i)
                             mv x$num.smi $i
                         end

cd ..
mkdir moses_results
cd split_moses

for i in (seq 1 193)
    sem -j +0 "java -jar PaDEL-Descriptor.jar -dir $i -file ../moses_results/moses$i.csv \
    -threads 1 -maxruntime 10000 -log ../moses_results/moses$i.log -retainorder -2d -3d"
end
sem --wait
