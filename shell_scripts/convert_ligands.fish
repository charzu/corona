mkdir -p data/mol2

for f in data/sdf/*sdf
    set outf (echo $f | sed 's sdf mol2 g')
    sem -j -1 timeout 120s obabel -isdf $f -omol2 -O$outf 2> /dev/null
end
sem --wait