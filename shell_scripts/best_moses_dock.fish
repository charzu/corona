set UTILPTH /home/charzewski/opt/mgltools/MGLToolsPckgs/AutoDockTools/Utilities24
mkdir -p data/moses/moses_dock_best

for i in (seq 5)
    obabel -ismi data/moses/moses_dock_best/$i.smi -opdb -Odata/moses/moses_dock_best/$i.tmp.pdb --gen3d -h
    obminimize data/moses/moses_dock_best/$i.tmp.pdb > data/moses/moses_dock_best/$i.pdb 2>/dev/null

    set outname (echo $i | sed 's pdb pdbqt ')
    pythonsh $UTILPTH/prepare_ligand4.py -l $i -o data/moses/moses_dock_best/$outname >>prep.out 2>>prep.err

    pythonsh $UTILPTH/prepare_dpf42.py -l data/ligands/moses_dock_best/$i.pdbqt \
        -r data/receptor/6lu7_rec_autopsf.pdbqt -o data/ligands/moses_dock_best/$i.dpf -i data/configs/screen.dpf

    sed -i "s|6lu7_rec_autopsf|data/receptor/maps/6lu7_rec_autopsf|" data/ligands/moses_dock_best/$i.dpf
    sed -i "s|^move $i|move data/ligands/moses_dock_best/$i|" data/ligands/moses_dock_best/$i.dpf
    autodock4 -p data/moses/moses_dock_best/$i\.dpf -l data/moses/moses_dock_best/$i\.dlg
 end
