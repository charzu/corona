set UTILPTH ~/opt/MGLTools-1.5.6/MGLToolsPckgs/AutoDockTools/Utilities24
mkdir -p data/pdbqt

for f in data/mol2/lig_*.mol2
    set outf (echo $f | sed 's mol2 pdbqt g')
    if not test -e $outf
        sem -j +0 timeout 60s ~/bin/pythonsh $UTILPTH/prepare_ligand4.py -l $f -o $outf #>> /dev/null 2>>/dev/null
    end
end
sem --wait