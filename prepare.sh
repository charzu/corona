#!/usr/bin/env bash
fish shell_scripts/download_ligands.fish
fish shell_scripts/convert_ligands.fish
fish shell_scripts/prepare_ligand.fish
fish shell_scripts/dock.fish
fish shell_scripts/summary_best.fish
fish shell_scripts/descriptors.fish
fish shell_scripts/split_moses.fish

