## Receptor
The receptor atoms from 6LU7 PDB file were extracted.
Hydrogens were added with autopsf program bundled in VMD 1.9.3 software 
using default parameters.
Receptor maps for autodock were prepared using autogrid4 program for all 
atomtypes available in standard forcefield.
Box size and location was determined manually with ADT. 
Since this is one-time computation the resulting files are available 
in data/receptor directory.

## Ligands preparation and docking 
265242 ligands from NCI were converted to mol2 format with openbabel 2.4.1 software 
utilizing addition of all necessary hydrogen atoms. 
Ligand structures were subsequently prepared for docking with preprare_ligand4.py 
script available in MGL-Tools 1.5.6 library. 
Ligands were docked with autodock4 software with 10 runs per ligand. 
Some ligands failed to dock due to lack of parameters for special atoms 
and were ignored in analysis.  
Only the best score, estimated Free Energy of Binding (FEoB) for each 
ligand was retained.


## Descriptors and predictive model 
PaDEL 2.21 software was applied to calculate 1D, 2D and 3D descriptors for all NCI ligands. 
As the mol2 structures were already prepared they were used as PaDEL input.   
SciKit-learn library was applied for machine learning. 
Positive scores from docking were truncated at 20 kcal/mol and used as predictive values.
100 descriptors selected with SelectKBest were utilized as features for a 
LinearRegression model. In 5-fold cross-validation the model acquired scores 
MSE= 0.5609 +- 0.0006 and 
MAE= 0.5005 +- 0.0009.
 
Trained model was applied for Free Energy of Binding prediction for 
larger set of ligands - the MOSES dataset containing 1936962 ligands in SMILES format. 
These ligands are of higher therapeutic potential as they were filtered due to logP, MCFs and PAINS.  
Their required descriptors were calculated with PaDEL and directed to FEoB prediction.

## Results
Five MOSES ligands with the lowest FEoB predicted by the model were converted and docked 
as described above. The lowest FEoB for them were: 
-7.58, -8.43, -8.54, -7.63 and -7.78 kcal/mol respectively.
Structures of best ligands are available in data/best directory. 
Selected ligands dock well in the receptor and they constitute proper leads for 
subsequent ligand design.


## How to run it
1. Install prerequirements
2. Adjust UTILPTH variable in shell scripts
3. Run prepare.sh
4. Run modeling.ipynb
5. Run validation.sh 